var baseURL = "http://localhost:8080/SHASBackend/jersey/"
var DOOR_NUM = 4;
function getDoorInfo() {
	$.get(baseURL + "doors", function(data) {
		for (var i = 0; i < DOOR_NUM; i++) {
			// alert(data[i].name + ", " + data[i].closed)
			$("#div_door" + i + "_name").html(data[i].name);
			var status = data[i].closed == true ? "closed" : "open";
			$("#div_door" + i + "_status").html(status);
		}
	});
}

function refreshTemp() {
	$.get(baseURL + "thermostat/temp", function(data) {
		$("#temp").val(data != -1 ? data : "off");
	});
}

function ThermostatOn() {
	$.ajax({
	    url: baseURL + 'thermostat/onoff/true',
	    type: 'PUT',
	    success: function(result) {
	    	// it should succeed
	    }
	});
}

function ThermostatOff() {
	$.ajax({
	    url: baseURL + 'thermostat/onoff/false',
	    type: 'PUT',
	    success: function(result) {
	    	// it should succeed
	    }
	});
}

function SetTemperature(temp) {
	$.ajax({
	    url: baseURL + 'thermostat/temp/' + temp,
	    type: 'PUT',
	    success: function(result) {
	    	// it should succeed
	    }
	});
}

function setOverride() {
	$.ajax({
	    url: baseURL + 'thermostat/override/true',
	    type: 'PUT',
	    success: function(result) {
	    	// it should succeed
	    }
	});
}

function releaseOverride() {
	alert("!!!");
	$.ajax({
	    url: baseURL + 'thermostat/override/false',
	    type: 'PUT',
	    success: function(result) {
	    	// it should succeed
	    }
	});
}

function onSetTemp() {
	setOverride();
	SetTemperature($("#tempToSet").val());
	setTimeout(releaseOverride, 10000);
}